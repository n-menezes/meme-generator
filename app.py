"""Return the flask app."""
import random
import os
import requests
import shutil
from flask import Flask, render_template, abort, request
from QuoteEngine.quote_engine import QuoteModel, Ingestor
from MemeGenerator.meme_generator import MemeEngine


app = Flask(__name__)
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0

meme = MemeEngine('./static/')


def setup():
    """Load all resources."""
    quote_files = ['./_data/DogQuotes/DogQuotesTXT.txt',
                   './_data/DogQuotes/DogQuotesDOCX.docx',
                   './_data/DogQuotes/DogQuotesPDF.pdf',
                   './_data/DogQuotes/DogQuotesCSV.csv']

    quotes = []
    for file in quote_files:
        quotes += Ingestor.parse(file)

    images_path = "./_data/photos/dog/"

    imgs = [
        images_path + file for file in os.listdir(images_path)
        if file.split('.')[-1] in ['jpg', 'jpeg', 'png']
        ]

    return quotes, imgs


quotes, imgs = setup()


@app.route('/')
def meme_rand():
    """Generate a random meme."""
    img = random.choice(imgs)
    quote = random.choice(quotes)
    path = meme.make_meme(img, quote.body, quote.author)
    return render_template('meme.html', path=path)


@app.route('/create', methods=['GET'])
def meme_form():
    """User input for meme information."""
    return render_template('meme_form.html')


@app.route('/create', methods=['POST'])
def meme_post():
    """Create a user defined meme."""
    form_data = request.form
    image_url = form_data['image_url']
    body = form_data['body']
    author = form_data['author']

    img = './temp.jpg'

    try:
        assert(image_url.split('.')[-1] in ['jpg', 'jpeg', 'png'])
        response = requests.get(image_url, stream=True)

        if response.status_code == 200:
            with open(img, 'wb') as outfile:
                shutil.copyfileobj(response.raw, outfile)

        response.close()
    except AssertionError:
        return render_template('meme_error.html')

    path = meme.make_meme(img, body, author)

    if os.path.exists(img):
        os.remove(img)

    return render_template('meme.html', path=path)


if __name__ == "__main__":
    app.run()
