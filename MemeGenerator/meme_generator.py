"""Return the output image path of the meme generated."""
from PIL import Image, ImageDraw, ImageFont
import random


class MemeEngine:
    """A meme engine adds quotes to images."""

    def __init__(self, output_dir):
        """Create a new MemeEngine.

        :param output_dir: the directory where to save the meme image.
        """
        self.output_dir = output_dir

    def make_meme(self, img_path, text, author, width=500) -> str:
        """Add quotes to images to create memes."""
        with open(img_path, "rb") as image:
            img = Image.open(image)
            img_width, img_height = img.size

            try:
                assert(width <= 500)
                if img_width > 500:
                    ratio = width/float(img_width)
                    height = int(ratio*float(img_height))
                    img = img.resize((width, height))
                    img_width, img_height = img.size
                else:
                    pass
            except AssertionError:
                raise Exception("The image width must be less than 500px.")

            # Choosing a random position for the quote
            x, y = int(random.randrange(img_width)*0.5), \
                int(random.randrange(img_height)*0.5)

            quote = f"{text} \n - {author}."

            # Picking a font size according to the space available
            for fnt_size in range(50, 0, -1):
                fnt = ImageFont.truetype("./font/arial.ttf", fnt_size)
                if fnt.getsize(quote)[0] <= (img_width - x):
                    break

            drawing_context = ImageDraw.Draw(img)
            drawing_context.text(
                (x, y), quote, font=fnt, stroke_width=2, stroke_fill='black')

            filename = 'image.png'
            output_img_path = self.output_dir + filename
            img.save(output_img_path)

        return output_img_path
