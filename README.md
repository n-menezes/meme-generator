# Generate Dog Memes 
At a high-level, the goal of this project is to create an engine that generates memes either by executing a flask application or the command-line interface tool. These memes are created by using images and text as data input. 


## Overview

Concretely, this project involves parsing text data from different file formats (text, pdf, docx and csv) as to retrieve readable quotes and and print them on image objects with the help of the `Pillow` module. After that we build the flask application and command-line tool so that we can accept dynamic user input too.


## Project Scaffolding

```
├── README.md               # This file.
├── requirements.txt        # Module requirements.
├── app.py                  # Flask application file.
├── meme.py                 # Command-Line Interface tool.
├── QuoteEngine
│   └── quote_engine.py     
├── MemeEngine
│   └── meme_engine.py
├── _data
│   ├── DogQuotes
│   └── photos
├── font
│   └── arial.ttf
├── static
│   └── image.png
├── templates
│   ├── base.html
│   ├── meme_form.html
│   └── meme.html
```

- `app.py`: This file contains a flask app script. The app uses the Quote Engine Module and Meme Generator Modules to generate a random captioned image. It uses the requests package to fetch an image from a user submitted URL.
- `meme.py`: The Python script that wraps the command-line tool, and invokes the `MemeEngine` and `QuoteModel` classes that generate the memes. In case any of the input arguments are missing then a random selection is used.
- `quote_engine.py`: This file contains the representation of a quote by means of a `QuoteModel` class, and the different strategy objects of the abstract class `IngestorInterface` used to ingest different file types, such as text and pdf.  
- `meme_engine.py`: This file contains the definition of the `MemeEngine` class which is used to modify and add quotes to images, i.e. make memes. 
- `templates`: These are HTML files used by Flask to render the webpages. 

The data files are located in the `_data/` folder.


## Project Interface 

### Command-Line Interface (CLI) tool

```
python
usage: meme.py [--path PATH] [--body QUOTE] [--author AUTHOR]

Generate memes by providing an image path, quote body and author.

positional arguments:
  --path     Path to image file.
  --body     String text.
  --author   String text.
```

#### Example

```
$ python3 meme.py --path "./_data/photos/dog/xander_1.jpg" --body 'Hello world!' --author 'Tester'
```
This line will generate the meme image `tmpimage.png` with the following quote "Hello world! - Tester". In case a body is provided but not an author, then an exception will be raised. On the other hand, if a body is provided but not an author then a random meme will be generated. 


### Flask application
```
$ python3 app.py 
```
- Run the python file to serve the flask app. 
- Use your web browser to run the server address provided by the application. 
- Navigate through the webpage to create random dog memes, or provide external data to customize your own!
