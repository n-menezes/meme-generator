"""Return the ingestor according to the file type provided."""
from abc import ABC, abstractmethod
from typing import List
import subprocess
import docx
import pandas as pd


class QuoteModel:
    """A quote model encapsulates the information of a quote."""

    def __init__(self, body: str, author: str):
        """Create a new QuoteModel.

        :param body: the quote text.
        :param author: the quote author.
        """
        self.body = body
        self.author = author

    def __repr__(self) -> str:
        """Return a computer-readable string representation of this object."""
        return f"QuoteModel(body={self.body!r}, author={self.author!r})"


class IngestorInterface(ABC):
    """The ingestor interface defines general properties of an ingestor.

    This is an abstract base class to be realized by strategy objects.
    """

    @classmethod
    def can_ingest(cls, path: str) -> bool:
        """Test if file types can be ingested."""
        extension = path.split('.')[-1]
        return extension in cls.file_types

    @classmethod
    @abstractmethod
    def parse(cls, path: str) -> List[QuoteModel]:
        """Parse text-like files."""
        pass

    @staticmethod
    def from_text_to_rows(text):
        """Transform a text string into lists."""
        lines = text.strip().replace(' "', ', "').replace('"', '').split(',')
        rows = map(lambda row: row.strip().split(" - "), lines)
        return rows


class CSVIngestor(IngestorInterface):
    """A strategy object of the IngestorInterface class.

    The CSVIngestor class parses csv files into a list of quotes.
    """

    file_types = ['csv']

    @classmethod
    def parse(cls, path: str) -> List[QuoteModel]:
        """Parse csv file types."""
        if not cls.can_ingest(path):
            raise Exception('Cannot ingest this file type.')

        df = pd.read_csv(path, sep=',', header='infer')

        try:
            assert(not df.empty)
            quotes = [QuoteModel(
                df[df.index == i].body[i],
                df[df.index == i].author[i])
                for i in range(0, len(df), 1)]
        except AssertionError:
            raise Exception("CSV file is empty.")

        return quotes


class PDFIngestor(IngestorInterface):
    """A strategy object of the IngestorInterface class.

    The PDFIngestor class parses pdf files into a list of quotes.
    The PDFIngestor uses a subprocess to retrieve data from pdf files.
    """

    file_types = ['pdf']

    @classmethod
    def parse(cls, path: str) -> List[QuoteModel]:
        """Parse pdf file types."""
        if not cls.can_ingest(path):
            raise Exception('Cannot ingest this file type.')

        process = subprocess.Popen(
            ["pdftotext", path, "-"], shell=False,
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        bytes_output, errors = process.communicate()
        text = bytes_output.decode("UTF-8")

        try:
            assert(len(text) > 0)
            quotes = [
                QuoteModel(row[0], row[1])
                for row in cls.from_text_to_rows(text)
                ]
        except AssertionError:
            raise Exception("Pdf file is empty.")

        return quotes


class DocxIngestor(IngestorInterface):
    """A strategy object of the IngestorInterface class.

    The DocxIngestor class parses docx files into a list of quotes.
    """

    file_types = ['docx']

    @classmethod
    def parse(cls, path: str) -> List[QuoteModel]:
        """Parse docx file types."""
        if not cls.can_ingest(path):
            raise Exception('Cannot ingest this file type.')

        document = docx.Document(path)
        text = ''
        for paragraph in document.paragraphs:
            text += paragraph.text + " "

        try:
            assert(len(text) > 0)
            quotes = [
                QuoteModel(row[0], row[1])
                for row in cls.from_text_to_rows(text)
                ]
        except AssertionError:
            raise Exception("Docx file is empty.")

        return quotes


class TextIngestor(IngestorInterface):
    """A strategy object of the IngestorInterface class.

    The TextIngestor class parses text files into a list of quotes.
    """

    file_types = ['txt']

    @classmethod
    def parse(cls, path: str) -> List[QuoteModel]:
        """Parse text file types."""
        if not cls.can_ingest(path):
            raise Exception('Cannot ingest this file type.')

        with open(path, mode='r') as txtfile:
            text = txtfile.read()

        try:
            assert(len(text) > 0)
            quotes = [
                QuoteModel(row[0], row[1])
                for row in cls.from_text_to_rows(text)
                ]
        except AssertionError:
            raise Exception("Text file is empty.")

        return quotes


class Ingestor(IngestorInterface):
    """A strategy object of the IngestorInterface class.

    The Ingestor encapsulates all different file ingestors into
    a single interface.
    The Ingestor evaluates when a file can be ingested, and then
    applies the right parser.
    """

    ingestors = [
        CSVIngestor,
        PDFIngestor,
        TextIngestor,
        DocxIngestor
        ]

    @classmethod
    def parse(cls, path: str) -> List[QuoteModel]:
        """Parse files according to their file type."""
        for ingestor in cls.ingestors:
            if ingestor.can_ingest(path):
                return ingestor.parse(path)
